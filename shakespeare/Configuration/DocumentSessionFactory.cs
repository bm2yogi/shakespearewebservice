﻿using System.Reflection;
using Raven.Abstractions.Data;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Indexes;

namespace Shakespeare.Configuration
{
    public static class DocumentSessionFactory
    {
        private static volatile IDocumentStore _documentStore;
        private static readonly object MyLock = new object();

        public static IDocumentStore DocumentStore
        {
            get
            {
                if (_documentStore == null)

                    lock(MyLock)
                        if (_documentStore == null) _documentStore = BuildDocumentStore();

                return _documentStore;
            }
        }

        private static IDocumentStore BuildDocumentStore()
        {
            var parser = ConnectionStringParser<RavenConnectionStringOptions>.FromConnectionStringName("RavenDBConnection");
            parser.Parse();

            var store = new DocumentStore { ApiKey = parser.ConnectionStringOptions.ApiKey, Url = parser.ConnectionStringOptions.Url }
                .Initialize();

            IndexCreation.CreateIndexes(Assembly.Load("Shakespeare"), store);

            return store;
        }

        public static IDocumentSession Create()
        {
            return DocumentStore.OpenSession();
        }
    }
}