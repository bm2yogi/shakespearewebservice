﻿using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using Shakespeare.Works;

public class Works_ByTitle : AbstractIndexCreationTask<Work>
{
    public Works_ByTitle()
    {
        Map = works =>
            works.Select(work => new { work.Title});

        Indexes.Add(work => work.Title, FieldIndexing.Analyzed);
        Analyzers.Add(work => work.Title, "Lucene.Net.Analysis.Standard.StandardAnalyzer");
    }
}