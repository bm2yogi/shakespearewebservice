using System.Collections.Generic;
using System.Linq;
using Raven.Client;
using Shakespeare.Configuration;

namespace Shakespeare.Works
{
    public interface IWorksRepository
    {
        IEnumerable<Work> GetAll();
        IEnumerable<Work> Search(string keywords);
        Work GetById(int id);
    }

    public class WorksRepository : IWorksRepository
    {
        private readonly IDocumentSession _session;
        public WorksRepository()
        {
            _session = DocumentSessionFactory.Create();
        }

        public IEnumerable<Work> GetAll()
        {
            using (var session = DocumentSessionFactory.Create())
            return session.Query<Work>().Select(work => new Work { Id = work.Id, Title = work.Title });
        }

        public IEnumerable<Work> Search(string keywords)
        {
            if (keywords == null)
                return new Work[0];

            using (var session = DocumentSessionFactory.Create())
            return session.Advanced
                .LuceneQuery<Work>("Works/ByTitle")
                .Search(x => x.Title, string.Join(" ", keywords))
                .Select(work => new Work {Id = work.Id, Title = work.Title});
        }

        public Work GetById(int id)
        {
            using (var session = DocumentSessionFactory.Create())
            return session.Load<Work>(id);
        }
    }
}