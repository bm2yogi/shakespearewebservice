﻿using Nancy;
using Nancy.Responses;
using Shakespeare.Works;

namespace Shakespeare.WebService.Searching
{
    public class SearchModule : NancyModule
    {
        public SearchModule(IWorksRepository worksRepository):base("search")
        {
            Get[@"/"] =
                o =>
                    {
                        var query = Request.Query;
                        return new JsonResponse(worksRepository.Search(query.Keywords), new DefaultJsonSerializer());
                    };
        }
    }
}