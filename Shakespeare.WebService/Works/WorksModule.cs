﻿using System;
using Nancy;
using Nancy.Json;
using Nancy.ModelBinding;
using Nancy.Responses;
using Shakespeare.Works;

namespace Shakespeare.WebService.Works
{
    public class WorksModule : NancyModule
    {
        public WorksModule(IWorksRepository worksRepository):base("works")
        {
            Get[@"/"] =
                o => Response.AsJson(worksRepository.GetAll());

            Get[@"/{id}"] =
                o =>
                    {
                        JsonSettings.MaxJsonLength = 1000000;
                        return new JsonResponse(worksRepository.GetById(o.id), new DefaultJsonSerializer());
                    };
        }
    }
}