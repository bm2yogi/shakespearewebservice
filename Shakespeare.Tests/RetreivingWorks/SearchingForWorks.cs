using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Sahara;
using Shakespeare.Works;

namespace Shakespeare.Tests.Unit.RetreivingWorks
{
    public class SearchingForWorksContext
    {
        protected string Keywords;
        protected IEnumerable<Work> SearchResults;

        protected void Given_a_set_of_matching_keywords()
        {
            Keywords = "Henry";
        }

        protected void Given_a_set_of_non_matching_keywords()
        {
            Keywords = "Jedi Sith Empire";
        }

        protected void When_searching_for_works_by_keywords()
        {
            SearchResults = new WorksRepository().Search(Keywords);
        }
    }

    [TestFixture]
    public class WhenMatchingTitlesAreFound : SearchingForWorksContext
    {
        [TestFixtureSetUp]
        public void SetupContext()
        {
            Given_a_set_of_matching_keywords();
            When_searching_for_works_by_keywords();
        }

        [Test]
        public void It_should_return_list_of_all_matching_works()
        {
            SearchResults.Count().ShouldEqual(7);
        }

        [Test]
        public void The_returned_titles_should_contain_one_or_more_keywords()
        {
            SearchResults.Select(s => s.Title).All(s => s.Contains("Henry")).ShouldBeTrue();
        }
    }

    [TestFixture]
    public class WhenNoMatchingTitlesAreFound : SearchingForWorksContext
    {
        [TestFixtureSetUp]
        public void SetupContext()
        {
            Given_a_set_of_non_matching_keywords();
            When_searching_for_works_by_keywords();
        }

        [Test]
        public void It_should_not_return_null()
        {
            SearchResults.ShouldNotBeNull();
        }

        [Test]
        public void It_should_return_list_of_all_matching_works()
        {
            SearchResults.Count().ShouldEqual(0);
        }
    }
}