﻿using NUnit.Framework;
using Sahara;
using Shakespeare.Works;

namespace Shakespeare.Tests.Unit.RetreivingWorks
{
    [TestFixture]
    public class GettingAListOfWorks
    {
        [Test]
        public void It_should_return_a_list_of_all_works()
        {
            new WorksRepository().GetAll().ShouldNotBeNull();
        }
    }
}